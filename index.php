<?php
/**
 * Created by PhpStorm.
 * User: r.wendlinger
 * Date: 12.01.18
 * Time: 13:35
 */

include "_baseurl.php";
?>
<!doctype html>
<html>
<head>
    <base href="<?=$baseurl?>">
</head>
<body>
<h1>
    Registration
</h1>
<h2>$_SERVER</h2>
<p>
    HTTP_HOST: "<?=$_SERVER['HTTP_HOST']?>" <br/>
    REQUEST_URI: "<?= $_SERVER['REQUEST_URI'] ?>"<br/>
    FULL_URL: <?=$fullurl?>"<br/>
    baseurl: "<?=$baseurl?>"
</p>
<h2>$_COOKIE</h2>
<pre>
    <?= print_r($_COOKIE) ?>
</pre>

<form action="registration.php" method="post" target="_blank">
    <label for="email">Email:</label>
    <input type="email" name="email" title="email"/><br/>
    <label for="action">Action (hidden)</label>
    <input type="hidden" value="createMail" name="action"/><br/>
    <label for="ref">refseite (hidden) "<?=urlencode($fullurl)?>"</label>
    <input type="hidden" value="<?=urlencode($fullurl)?>" name="ref"/><br>
    <input type="submit" title="Submit"/>
</form>
</body>
</html>
